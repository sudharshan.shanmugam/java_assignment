package Automation;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWorkShopMobiles {

    public static void main(String[] args) throws InterruptedException {
        // TODO Auto-generated method stub

        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demowebshop.tricentis.com/login");
        driver.findElement(By.id("Email")).sendKeys("sudhar3298@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("Sudhar@4200");
        driver.findElement(By.id("RememberMe")).click();
        driver.findElement(By.xpath("//input[@value='Log in']")).click();
        WebElement Electronics=    driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));
        Actions act=new Actions(driver);
        act.moveToElement(Electronics).build().perform();
        act.moveToElement(driver.findElement(By.partialLinkText("Cell phones"))).click().perform();
        driver.findElement(By.xpath("//input[@value='Add to cart'][1]")).click();
        driver.findElement(By.linkText("Shopping cart")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("termsofservice")).click();
        driver.findElement(By.id("checkout")).click();
        WebElement Address=driver.findElement(By.id("billing-address-select"));
        Select seladdress=new Select(Address);
        seladdress.selectByVisibleText("New Address");
        WebElement Country=    driver.findElement(By.id("BillingNewAddress_CountryId"));
        Select sel=new Select(Country);
        sel.selectByVisibleText("India");
        driver.findElement(By.id("BillingNewAddress_City")).sendKeys("Krishnagiri");
        driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("2/1,Anna Nagar");
        driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("635104");
        driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("9626325556");
        driver.findElement(By.xpath("//li[@class='tab-section allow active']//input[@title='Continue']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("shippingoption_2")).click();
        driver.findElement(By.xpath("//input[@onclick='ShippingMethod.save()']")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("paymentmethod_2")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
        Thread.sleep(3000);
        WebElement Credit=driver.findElement(By.id("CreditCardType"));
        Select Cred=new Select(Credit);
        Cred.selectByIndex(1);
        driver.findElement(By.id("CardholderName")).sendKeys("Sudharshan");
        driver.findElement(By.id("CardNumber")).sendKeys("1234567890901212");
        driver.findElement(By.id("CardCode")).sendKeys("456");
        driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']")).click();
        Thread.sleep(3000);
        JavascriptExecutor js= (JavascriptExecutor)driver;           
        js.executeScript("document.getElementById('confirm-order-buttons-container').scrollIntoView()");    
        driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']")).click();
        Thread.sleep(3000);
        WebElement text= driver.findElement(By.xpath("//div[@class='section order-completed']"));       
        System.out.println(text.getText());
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(3000);
        driver.quit();
 


    }
	
	
}
